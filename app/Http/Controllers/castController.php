<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));
    }

    public function store(Request $request)
    {
        // dd($request->all()); untuk cek data seperti vardump
        $request->validate([
            'namaCast' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:100'

        ], [
            'namaCast.required' => 'Nama Harus di isi !',
            'umur.required' => 'umur Harus di isi !',
            'bio.required' => 'bio Harus di isi !',
            'bio.max' => 'max character 100 !',
        ]);

        DB::table('cast')->insert([
            [
                'nama' => $request['namaCast'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
                'created_at' => now()

            ]
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all()); untuk cek data seperti vardump
        $request->validate([
            'namaCast' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:100'

        ], [
            'namaCast.required' => 'Nama Harus di isi !',
            'umur.required' => 'umur Harus di isi !',
            'bio.required' => 'bio Harus di isi !',
            'bio.max' => 'max character 100 !',
        ]);
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['namaCast'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
                'updated_at' => now()
            ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        // dd($request->all()); untuk cek data seperti vardump
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
