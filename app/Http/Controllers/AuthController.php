<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi()
    {
        return view('halaman.registrasi');
    }

    public function signUp(Request $request)
    {
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];

        return view('halaman.welcome', compact('first_name', 'last_name'));
    }
}
