<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/data-table', function () {
    return view('table.data-table');
});

Route::get('/table', function () {
    return view('table.table');
});


Route::get('/registrasi', [AuthController::class, 'registrasi']);
Route::post('/signUp', [AuthController::class, 'signUp']);

Route::get('/cast', [castController::class, 'index']);
Route::get('/cast/{cast_id}', [castController::class, 'show']);
Route::get('/cast/create', [castController::class, 'create']);
Route::post('/cast', [castController::class, 'store']);
Route::get('/cast/{cast_id}/edit', [castController::class, 'edit']);
Route::put('/cast/{cast_id}', [castController::class, 'update']);
Route::delete('/cast/{cast_id}', [castController::class, 'destroy']);
