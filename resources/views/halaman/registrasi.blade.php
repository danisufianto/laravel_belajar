@extends('layout.master');
@section('judul')
Halaman Registrasi
@endsection()

@section('isi')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/signUp" method="POST">
        @csrf
        <fieldset>
            <label for="first_name">First Name: </label>
            <br /><br />
            <input type="text" name="first_name" id="first_name" />
            <br /><br />

            <label for="last_name">Last Name: </label>
            <br /><br />

            <input type="text" name="last_name" id="last_name" />
            <br /><br />
            <label for="gender">Gender:</label><br /><br />

            <input type="radio" name="gender" id="gender" value="Male" />
            <label for="Male">Male </label><br />

            <input type="radio" name="gender" id="gender" value="Female" />
            <label for="Female">Female </label>
            <br />

            <input type="radio" name="gender" id="gender" value="Other" />
            <label for="Other">Other </label>
            <br />

            <label for="nationaly">Nationaly:</label><br /><br />
            <select name="nationaly" id="nationaly">
                <option value="Indonesia">Indonesia</option>
                <option value="Singapore">Singapore</option>
                <option value="Thailand">Thailand</option>
            </select>

            <br /><br />

            <label for="languange">Languange Spoken:</label><br /><br />

            <input type="checkbox" name="languange1" id="languange1" value="Bahasa Indonesia" />
            <label for="languange1">Bahasa Indonesia </label>
            <br />

            <input type="checkbox" name="languange2" id="languange2" value="English" />
            <label for="languange2">English </label><br />

            <input type="checkbox" name="languange3" id="languange3" value="Other" />
            <label for="languange3">Other </label>
            <br /><br />

            <label for="bio">Bio :</label><br /><br />
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br /><br /><br />
            <input type="submit" value="Sign Up" />
        </fieldset>
    </form>
@endsection()