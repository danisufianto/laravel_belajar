@extends('layout.master');
@section('judul')
Edit Cast {{$cast->nama}}
@endsection()

@section('isi')

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
  <div class="form-group">
    <label >Nama Cast </label>
    <input name="namaCast" value="{{$cast->nama}}" type="text" class="form-control" >
  </div>
  @error('namaCast')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
   <div class="form-group">
    <label >Umur: </label>
    <input name="umur" value="{{$cast->umur}}" type="number" class="form-control" >
  </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
   <div class="form-group">
    <label >Bio  </label>
    <textarea name="bio"  cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
  </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Update</button>
</form>


@endsection()