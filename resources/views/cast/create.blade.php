@extends('layout.master');
@section('judul')
Tambah Cast
@endsection()

@section('isi')

<form action="/cast" method="post">
    @csrf
  <div class="form-group">
    <label >Nama Cast </label>
    <input name="namaCast" type="text" class="form-control" >
  </div>
  @error('namaCast')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
   <div class="form-group">
    <label >Umur: </label>
    <input name="umur" type="number" class="form-control" >
  </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
   <div class="form-group">
    <label >Bio  </label>
    <textarea name="bio"  cols="30" rows="10" class="form-control"></textarea>
  </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Tambah</button>
</form>


@endsection()